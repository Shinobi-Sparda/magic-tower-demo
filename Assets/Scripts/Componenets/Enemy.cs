using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : LivingEntity
{
    
    [SerializeField] private float moveSpeed = 15;
    [SerializeField] private float stopDistance = 5;
    [SerializeField] private float attackRate = 1;
    [SerializeField] private float damage = 10;

    private float currentattackTime;

    public EnemyTypes enemyType;
    private Tower target;

    public void Init(Transform _target)
    {
        currentHealth = maxHealth;
        target = _target.GetComponent<Tower>();
    }

    void Update()
    {
        if (GameManager.instance.isGameover || target == null)
            return;

        if (Vector3.Distance(transform.position, target.transform.position) > stopDistance)
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, moveSpeed * Time.deltaTime);
        else
        {
            if (Time.time > currentattackTime)
            {
                target.TakeDamage(damage);
                currentattackTime = Time.time + attackRate;
            }
        }
    }

    public override void DestroyEntity()
    {
        base.DestroyEntity();
        target = null;
        EnemySpawnManager.instance.RecycleEnemy(this);
    }
}
