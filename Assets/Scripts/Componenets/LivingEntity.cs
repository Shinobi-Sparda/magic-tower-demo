using System.Collections;
using UnityEngine;

public class LivingEntity : MonoBehaviour
{
    public float maxHealth = 100;
    internal float currentHealth;
    public MeshRenderer renderObject;
    private Color currentColor;

    public virtual void Start()
    {
        currentColor = renderObject.material.color;
    }

    public virtual void TakeDamage(float dmgAmt)
    {
        currentHealth = Mathf.Clamp(currentHealth - dmgAmt, 0, maxHealth);
        StopAllCoroutines();
        StartCoroutine(DamageFlash());
        if (currentHealth <= 0)
        {
            DestroyEntity();
        }
    }

    private IEnumerator DamageFlash()
    {
        renderObject.material.color = Color.red;
        yield return new WaitForSeconds(0.1f);
        renderObject.material.color = currentColor;
    }

    public virtual void DestroyEntity()
    {
        renderObject.material.color = currentColor;

    }
}
