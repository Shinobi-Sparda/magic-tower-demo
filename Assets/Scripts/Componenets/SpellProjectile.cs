using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellProjectile : MonoBehaviour
{
    [SerializeField] private ParticleSystem spellVfx;
    public float spellDamage;
    public float spellSpeed;
    private Transform target;
    private bool fired = false;
    public void Fire (Transform _target)
    {
        target = _target;
        fired = true;
    }

    void Update()
    {
        if (GameManager.instance.isGameover || !target.gameObject.activeSelf && fired)
        {
            Destroy(gameObject);
            return;
        }
            

        if (Vector3.Distance(transform.position, target.transform.position) > 0.1f)
            transform.position = Vector3.MoveTowards(transform.position, target.transform.position, spellSpeed * Time.deltaTime);
        else
        {
            target.GetComponent<Enemy>().TakeDamage(spellDamage);
            DestroyItem();
        }
    }

    private void DestroyItem()
    {
        Destroy(gameObject);
        Instantiate(spellVfx, transform.position, Quaternion.identity);
    }
}
