using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Tower;


[Serializable]
public class Spell
{
    public TowerSpells spell;
    public SpellProjectile spellProjectile;
    public float spellCoolDown;

    internal float currentCoolDownTime;
}

public class Tower : LivingEntity
{
    public enum TowerSpells
    {
        FireBall,
        Barrage
    }

    public float damage = 30;

    [SerializeField] private Spell[] spells;
    [SerializeField] private Transform spellShotPoint;

    public override void Start()
    {
        base.Start();
        currentHealth = maxHealth;
        GameManager.instance.tower = this;
    }

    private void Update()
    {
        if (GameManager.instance.isGameover || EnemySpawnManager.instance.spawnedEnemies.Count == 0)
            return;

        foreach (var spell in spells)
        {
            if (spell.currentCoolDownTime <= Time.time)
                CastSpell(spell);
        }
    }

    private void CastSpell(Spell spell)
    {
        SpellProjectile newSpell = Instantiate(spell.spellProjectile, spellShotPoint.position, Quaternion.identity);
        newSpell.Fire(EnemySpawnManager.instance.FindClosestEnemy(transform));
        spell.currentCoolDownTime = spell.spellCoolDown + Time.time;
    }

    public override void TakeDamage(float dmgAmt)
    {
        base.TakeDamage(dmgAmt);
        GameManager.instance.uiManager.OnTowerHealthUpdated?.Invoke(currentHealth);
        if (currentHealth <= 0)
            GameManager.instance.GameOver();
    }
}
