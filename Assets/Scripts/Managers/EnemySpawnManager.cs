﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum EnemyTypes
{
    Default,
    Fast,
    BigAndSlow
}

[Serializable]
public class EnemySpawnSet
{
    public EnemyTypes enemyType;
    public Enemy enemyObj;
    [Range(0f, 100f)]
    public int spawnChance;
}


public class EnemySpawnManager : MonoBehaviour
{
    [SerializeField] private EnemySpawnSet[] enemySpawnSets;
    [SerializeField] private float spawnInterval = 1f;
    [SerializeField] private float spawnOffset = 10f;

    internal List<Enemy> spawnedEnemies = new List<Enemy>();
    private Dictionary<EnemyTypes, Queue<Enemy>> enemyPool = new Dictionary<EnemyTypes, Queue<Enemy>>();

    private Transform towerTransform;
    public static EnemySpawnManager instance;

    private float spawnTimer;
    private float difficultyIncreaseTimer;
    [SerializeField] private float spawnTimerDecreaseValue = 0.25f;
    [SerializeField] private float difficultyIncreaseIntervals = 60f;

    void Start()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);

        towerTransform = GameManager.instance.tower.transform;
        enemySpawnSets.OrderBy(x => x.spawnChance);
    }

    void Update()
    {
        if (GameManager.instance.isGameover)
            return;

        //For spawning enemies after a time interval
        spawnTimer += Time.deltaTime;

        if (spawnTimer >= spawnInterval)
        {
            SpawnEnemy();
        }

        //For Increaseing Difficulty over tim
        difficultyIncreaseTimer += Time.deltaTime;

        if (difficultyIncreaseTimer >= difficultyIncreaseIntervals)
        {
            spawnInterval -= spawnTimerDecreaseValue;
            difficultyIncreaseTimer = 0;
        }
    }

    private void SpawnEnemy()
    {
        foreach (var item in enemySpawnSets)
        {
            if (UnityEngine.Random.Range(0, 101) <= item.spawnChance)
            {
                Enemy newEnemy = GetEnemy(item.enemyType);
                newEnemy.transform.position = GetRandomPointAroundObject(towerTransform.position, spawnOffset);
                newEnemy.transform.rotation = Quaternion.identity;
                newEnemy.Init(towerTransform);
                spawnedEnemies.Add(newEnemy);
                spawnTimer = 0f;
                break;
            }
        }
    }

    private Enemy GetEnemy(EnemyTypes enemyType)
    {
        Enemy ins;
        if (enemyPool.ContainsKey(enemyType) && enemyPool[enemyType].Count > 0)
        {
            ins = enemyPool[enemyType].Dequeue();
            ins.gameObject.SetActive(true);
        }
        else
            ins = Instantiate(enemySpawnSets.Where(x => x.enemyType == enemyType).FirstOrDefault().enemyObj);

        return ins;
    }

    public void RecycleEnemy(Enemy enemy)
    {
        enemy.gameObject.SetActive(false);
        spawnedEnemies.Remove(enemy);

        if (!enemyPool.ContainsKey(enemy.enemyType))
            enemyPool.Add(enemy.enemyType, new Queue<Enemy>());

        enemyPool[enemy.enemyType].Enqueue(enemy);
    }

    public Vector3 GetRandomPointAroundObject(Vector3 center, float radius)
    {
        float angle = UnityEngine.Random.Range(0f, Mathf.PI * 2f);
        float x = center.x + radius * Mathf.Cos(angle);
        float z = center.z + radius * Mathf.Sin(angle);

        return new Vector3(x, center.y, z);
    }

    public Transform FindClosestEnemy(Transform source)
    {
        float distanceToClosestEnemy = Mathf.Infinity;
        Transform closestEnemy = null;

        foreach (Enemy currentEnemy in spawnedEnemies)
        {
            float distanceToEnemy = (currentEnemy.transform.position - source.position).sqrMagnitude;
            if (distanceToEnemy < distanceToClosestEnemy)
            {
                distanceToClosestEnemy = distanceToEnemy;
                closestEnemy = currentEnemy.transform;
            }
        }
        return closestEnemy;
    }
}
