using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Tower tower;
    public UIManager uiManager;

    public static GameManager instance;

    public bool isGameover = false;

    public void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
    }

    public void GameOver()
    {
        isGameover = true;
        uiManager.gameOverObject.SetActive(true);
    }
}
