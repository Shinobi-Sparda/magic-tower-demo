using System;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Image towerHealthBar;
    [SerializeField] private Button spellButtonPrefab;
    public GameObject gameOverObject;

    public Action<float> OnTowerHealthUpdated;


    private void Start()
    {
        OnTowerHealthUpdated += UpdateHealthUI;
    }

    private void UpdateHealthUI(float health)
    {
        towerHealthBar.fillAmount = health/GameManager.instance.tower.maxHealth;
    }
}
